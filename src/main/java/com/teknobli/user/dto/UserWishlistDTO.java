package com.teknobli.user.dto;

import com.teknobli.user.entity.UserDetails;


public class UserWishlistDTO {

    private String userWishlistId;
    private String userId;
    private String productId;

    public String getUserWishlistId() {
        return userWishlistId;
    }

    public void setUserWishlistId(String userWishlistId) {
        this.userWishlistId = userWishlistId;
    }

//    public UserDetails getUserId() {
//        return userId;
//    }
//
//    public void setUserId(UserDetails userId) {
//        this.userId = userId;
//    }


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }
}
