package com.teknobli.user.service.impl;

import com.teknobli.user.entity.UserDetails;
import com.teknobli.user.repository.UserDetailsRepository;
import com.teknobli.user.service.UserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    UserDetailsRepository userDetailsRepository;

    @Override
    public UserDetails add(UserDetails userDetails) {
        return userDetailsRepository.save(userDetails);
    }

    @Override
    public void delete(String userId) {
        userDetailsRepository.delete(userId);
    }

    @Override
    public UserDetails update(UserDetails userDetails) {
        return userDetailsRepository.save(userDetails);
    }

    @Override
    public List<UserDetails> find() {

        List<UserDetails> employeeList = new ArrayList<>();
        Iterable<UserDetails> employeeIterable = userDetailsRepository.findAll();
        employeeIterable.forEach(employeeList::add);
        return employeeList;

    }

    @Override
    public UserDetails findOne(String userId) {
        return userDetailsRepository.findOne(userId);
    }
}
