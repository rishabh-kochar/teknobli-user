package com.teknobli.user.service.impl;

import com.teknobli.user.ProductMicroservice.Endpoints;
import com.teknobli.user.dto.ProductDTO;
import com.teknobli.user.entity.UserWishlist;
import com.teknobli.user.repository.UserWishlistRepository;
import com.teknobli.user.service.UserWishlistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserWishlistImpl implements UserWishlistService {

    @Autowired
    UserWishlistRepository userWishlistRepository;

    @Override
    public UserWishlist add(UserWishlist userWishlist) {

        UserWishlist userWishlist1 = null;
        try{
            if(userWishlistRepository.checkIfProductExists(userWishlist.getUserId(),userWishlist.getProductId())==0){
                userWishlist1 = userWishlistRepository.save(userWishlist);
                }
        }catch(Exception e){

        }
        return userWishlist1;
    }

    @Override
    public void delete(String userWishlistId) {
        userWishlistRepository.delete(userWishlistId);
    }

    @Override
    public List<ProductDTO> findByUserId(String userId) {

        List<ProductDTO> productDTOList = new ArrayList<>();
        for(UserWishlist userWishlist :userWishlistRepository.findByUserId(userId) ){
            ProductDTO productDTO = getProduct(userWishlist.getProductId());
            productDTO.getAttributes().put("wishListId",userWishlist.getUserWishlistId());
            productDTOList.add(productDTO);
        }
        return productDTOList;

    }

    public ProductDTO getProduct(String productId) {
        RestTemplate restTemplate = new RestTemplate();
        System.out.println(productId);
        ProductDTO result = restTemplate.getForObject(Endpoints.BASE_URL + Endpoints.SINGLRPRODUCT_URL + productId, ProductDTO.class);
        if (result != null) {
            return result;
        }
        return null;
    }
}
