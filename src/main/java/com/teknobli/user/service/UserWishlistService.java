package com.teknobli.user.service;

import com.teknobli.user.dto.ProductDTO;
import com.teknobli.user.entity.UserDetails;
import com.teknobli.user.entity.UserWishlist;

import java.util.List;

public interface UserWishlistService {

    public UserWishlist add(UserWishlist userWishlist);
    public void delete(String userWishlistId);
    public List<ProductDTO> findByUserId(String userId);
}
