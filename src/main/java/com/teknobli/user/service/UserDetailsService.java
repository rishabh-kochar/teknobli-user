package com.teknobli.user.service;

import com.teknobli.user.entity.UserDetails;

import java.util.List;

public interface UserDetailsService {


    public UserDetails add(UserDetails userDetails);
    public void delete(String userId);
    public UserDetails update(UserDetails userDetails);
    public List<UserDetails> find();
    public UserDetails findOne(String userId);
}
