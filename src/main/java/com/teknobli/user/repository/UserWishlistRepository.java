package com.teknobli.user.repository;

import com.teknobli.user.entity.UserWishlist;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserWishlistRepository extends CrudRepository<UserWishlist, String> {

    @Query("FROM UserWishlist WHERE user_id = ?1")
    List<UserWishlist> findByUserId(String userId);

    @Query("SELECT COUNT(*) FROM UserWishlist WHERE user_id = ?1 AND productId = ?2")
    Integer checkIfProductExists(String userId, String productId);
}
