package com.teknobli.user.entity;


import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name=UserDetails.TABLE_NAME)
public class UserDetails {

    public static final String TABLE_NAME = "USERDETAILS";
    private static final String ID_COLUMN = "USER_ID";

    @Id
    @Column(name=UserDetails.ID_COLUMN)
    private String userId;
    private String address;
    private String phoneNo;


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

}
