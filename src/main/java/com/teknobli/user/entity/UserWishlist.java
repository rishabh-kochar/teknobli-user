package com.teknobli.user.entity;


import com.fasterxml.jackson.annotation.JsonBackReference;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name=UserWishlist.TABLE_NAME)
public class UserWishlist {

    public static final String TABLE_NAME = "USERWISHLIST";
    private static final String ID_COLUMN = "USER_WISHLIST_ID";

    @Id
    @Column(name=UserWishlist.ID_COLUMN)
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name="uuid",strategy = "uuid2")
    private String userWishlistId;

//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name ="user_id")
//    private UserDetails userId;
    private String userId;
    private String productId;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getUserWishlistId() {
        return userWishlistId;
    }

    public void setUserWishlistId(String userWishlistId) {
        this.userWishlistId = userWishlistId;
    }

//    public UserDetails getUserId() {
//        return userId;
//    }
//
//    public void setUserId(UserDetails userId) {
//        this.userId = userId;
//    }


    public static String getTableName() {
        return TABLE_NAME;
    }

    public static String getIdColumn() {
        return ID_COLUMN;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "UserWishlist{" +
                "userWishlistId='" + userWishlistId + '\'' +
                ", userId='" + userId + '\'' +
                ", productId='" + productId + '\'' +
                '}';
    }
}
