package com.teknobli.user.controller;


import com.teknobli.user.dto.UserDetailsDTO;
import com.teknobli.user.dto.UserWishlistDTO;
import com.teknobli.user.entity.UserDetails;
import com.teknobli.user.service.UserDetailsService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
@CrossOrigin
public class UserDetailsController {

    @Autowired
    UserDetailsService userDetailsService;

    @CrossOrigin
    @RequestMapping(value = "/add",method = RequestMethod.POST )
    public ResponseEntity<UserDetails> addUser(@RequestBody UserDetailsDTO userDetailsDTO){
        UserDetails userDetails = new UserDetails();
        BeanUtils.copyProperties(userDetailsDTO, userDetails);
        UserDetails userCreated = userDetailsService.add(userDetails);
        return new ResponseEntity<>(userCreated,HttpStatus.CREATED);
    }

    @CrossOrigin
    @RequestMapping(value = "/update",method = RequestMethod.PUT)
    public ResponseEntity<UserDetails> updateUser(@RequestBody UserDetailsDTO userDetailsDTO){
        UserDetails userDetails = new UserDetails();
        BeanUtils.copyProperties(userDetailsDTO, userDetails);
        UserDetails employeeCreated = userDetailsService.update(userDetails);
        return new ResponseEntity<>(employeeCreated,HttpStatus.CREATED);
    }

    @CrossOrigin
    @RequestMapping(value = "/get/{userId}",method = RequestMethod.GET)
    public UserDetails findOne(@PathVariable("userId") String userId){
        return userDetailsService.findOne(userId);
    }
}
