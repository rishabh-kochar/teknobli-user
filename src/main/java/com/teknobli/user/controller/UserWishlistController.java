package com.teknobli.user.controller;


import com.teknobli.user.dto.ProductDTO;
import com.teknobli.user.dto.UserDetailsDTO;
import com.teknobli.user.dto.UserWishlistDTO;
import com.teknobli.user.entity.UserWishlist;
import com.teknobli.user.service.UserWishlistService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/wishlist")
@CrossOrigin
public class UserWishlistController {

    @Autowired
    UserWishlistService userWishlistService;

    @CrossOrigin
    @RequestMapping(value = "/add",method = RequestMethod.POST)
    public ResponseEntity<UserWishlist> addToWishlist(@RequestBody UserWishlistDTO userWishlistDTO){
        UserWishlist userWishlist = new UserWishlist();
        BeanUtils.copyProperties(userWishlistDTO, userWishlist);
        UserWishlist wishlistCreated = userWishlistService.add(userWishlist);
        return new ResponseEntity<>(wishlistCreated,HttpStatus.CREATED);
    }

    @CrossOrigin
    @RequestMapping(value = "/delete/{userWishListId}",method = RequestMethod.DELETE)
    public ResponseEntity<Boolean> deleteWishlist(@PathVariable("userWishListId") String userWishlistId){
        userWishlistService.delete(userWishlistId);
        return new ResponseEntity<>(Boolean.TRUE,HttpStatus.OK);
    }

    @CrossOrigin
    @RequestMapping(value = "/findbyuser/{userId}",method = RequestMethod.GET)
    public List<ProductDTO> findByUserId(@PathVariable("userId") String userId){
        return userWishlistService.findByUserId(userId);
    }




}
